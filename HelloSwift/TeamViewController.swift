//
//  TeamViewController.swift
//  HelloSwift
//
//  Created by Haldun Bayhantopcu on 03/06/14.
//  Copyright (c) 2014 fazlamesai. All rights reserved.
//

import UIKit

class TeamViewController: UIViewController {
  var team: Team?
  @IBOutlet var logoView : UIImageView

  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = "\(team?.city) \(team?.name)"
    
    // Load logo
    if let teamLogoPath = team?.logoPath {
      let config = NSURLSessionConfiguration.defaultSessionConfiguration()
      config.requestCachePolicy = NSURLRequestCachePolicy.ReturnCacheDataElseLoad
      let session = NSURLSession(configuration: config)
      let url = NSURL.URLWithString(teamLogoPath)
      let request = NSMutableURLRequest(URL: url)
      request.addValue("image/*", forHTTPHeaderField: "Accept")
      
      let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
        if error {
          return
        }
        
        let httpResponse = response as NSHTTPURLResponse
        let image = UIImage(data: data)
        dispatch_async(dispatch_get_main_queue(), {
          self.logoView.image = image
        })
      })
      task.resume()
    }
  }
}
