//
//  Team.swift
//  HelloSwift
//
//  Created by Haldun Bayhantopcu on 03/06/14.
//  Copyright (c) 2014 fazlamesai. All rights reserved.
//

import UIKit

struct Team {
  let name: String
  let city: String
  var logoPath: String?
  
  init(name: String, city: String) {
    self.name = name
    self.city = city
  }
}

