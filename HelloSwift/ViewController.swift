//
//  ViewController.swift
//  HelloSwift
//
//  Created by Haldun Bayhantopcu on 02/06/14.
//  Copyright (c) 2014 fazlamesai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  @IBOutlet var label : UILabel
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  @IBAction func buttonTapped(sender : AnyObject) {
    label.text = "Hello world!"
  }
}

