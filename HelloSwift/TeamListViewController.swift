//
//  TeamListViewController.swift
//  HelloSwift
//
//  Created by Haldun Bayhantopcu on 03/06/14.
//  Copyright (c) 2014 fazlamesai. All rights reserved.
//

import UIKit

class TeamListViewController: UITableViewController {
  var teams: Team[] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    teams = [
      Team(name: "Celtics", city: "Boston"),
      Team(name: "Mavericks", city: "Dallas"),
      Team(name: "Nets", city: "Brooklyn"),
      Team(name: "Rockets", city: "Houston"),
      Team(name: "Knicks", city: "New York"),
      Team(name: "Grizzlies", city: "Memphis"),
      Team(name: "76ers", city: "Philadelphia"),
      Team(name: "Pelicans", city: "New Orleans"),
      Team(name: "Raptors", city: "Toronto"),
      Team(name: "Spurs", city: "San Antonio"),
      Team(name: "Bulls", city: "Chicago"),
      Team(name: "Nuggets", city: "Denver"),
      Team(name: "Cavaliers", city: "Cleveland"),
      Team(name: "Timberwolves", city: "Minnesota"),
      Team(name: "Pistons", city: "Detroit"),
      Team(name: "Thunder", city: "Oklahoma City"),
      Team(name: "Pacers", city: "Indiana"),
      Team(name: "Trail Blazers", city: "Portland"),
      Team(name: "Bucks", city: "Milwaukee"),
      Team(name: "Jazz", city: "Utah"),
      Team(name: "Hawks", city: "Atlanta"),
      Team(name: "Warriors", city: "Golden State"),
      Team(name: "Hornets", city: "Charlotte"),
      Team(name: "Clippers", city: "Los Angeles"),
      Team(name: "Heat", city: "Miami"),
      Team(name: "Lakers", city: "Los Angeles"),
      Team(name: "Magic", city: "Orlando"),
      Team(name: "Suns", city: "Phoenix"),
      Team(name: "Wizards", city: "Washington"),
      Team(name: "Kings", city: "Sacramento"),
    ]
    teams[0].logoPath = "http://2.bp.blogspot.com/--QaZqovsU-4/TchCfurpfcI/AAAAAAAAAAs/dHWPyzOWG2o/s1600/Boston_Celtics+logo.gif"
  }

  // #pragma mark - Table view data source

  override func tableView(tableView: UITableView?, numberOfRowsInSection section: Int) -> Int {
    return teams.count
  }

  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let team = teams[indexPath.row]
    let cell = tableView.dequeueReusableCellWithIdentifier("TeamCell", forIndexPath: indexPath) as UITableViewCell
    cell.textLabel.text = team.name
    cell.detailTextLabel.text = team.city
    return cell
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
    if segue.identifier == "ShowTeam" {
      if segue.destinationViewController is TeamViewController {
        let viewController = segue.destinationViewController as TeamViewController
        viewController.team = teams[tableView.indexPathForSelectedRow().row]
      }
    }
  }

  /*
  // Override to support conditional editing of the table view.
  override func tableView(tableView: UITableView?, canEditRowAtIndexPath indexPath: NSIndexPath?) -> Bool {
      // Return NO if you do not want the specified item to be editable.
      return true
  }
  */

  /*
  // Override to support editing the table view.
  override func tableView(tableView: UITableView?, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath?) {
      if editingStyle == .Delete {
          // Delete the row from the data source
          tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
      } else if editingStyle == .Insert {
          // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
      }    
  }
  */

  /*
  // Override to support rearranging the table view.
  override func tableView(tableView: UITableView?, moveRowAtIndexPath fromIndexPath: NSIndexPath?, toIndexPath: NSIndexPath?) {

  }
  */

  /*
  // Override to support conditional rearranging of the table view.
  override func tableView(tableView: UITableView?, canMoveRowAtIndexPath indexPath: NSIndexPath?) -> Bool {
      // Return NO if you do not want the item to be re-orderable.
      return true
  }
  */

  /*
  // #pragma mark - Navigation

  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue?, sender: AnyObject?) {
      // Get the new view controller using [segue destinationViewController].
      // Pass the selected object to the new view controller.
  }
  */

}
